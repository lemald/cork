#lang racket

(require "cork.rkt"
	 web-server/servlet-env)

(serve/servlet start
	       #:command-line? #t
	       #:stateless? #t
	       #:servlet-regexp #rx""
	       #:file-not-found-responder file-not-found-handler)
