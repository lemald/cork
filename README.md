# An Experimental Web Application in Racket

By Eddie Maldonado

eddie (at) lemald (dot) org

This is an exercise in teaching myself basic principles of web
development and Racket. It is far from functional and intended for
educational / curiosity purposes only, though my hope is that it will
eventually become a useful toy, as opposed to just a toy.
